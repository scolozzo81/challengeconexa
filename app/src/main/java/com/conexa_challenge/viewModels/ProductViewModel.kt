package com.conexa_challenge.viewModels


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.conexa_challenge.db.entities.Product
import com.conexa_challenge.repository.services.interfaces.Repository
import com.conexa_challenge.utils.replace
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    var products = MutableLiveData<List<Product>>()
    var categories = MutableLiveData<List<String>>()
    var overview  = MutableLiveData<Pair<Double, Int>>()
    private var currentCategory=""
    private var isFirstCategory =true
    fun loadProducts() {
        viewModelScope.launch {
            var list = repository.getAllProducts()
            products.value = updateProducts(list)!!
        }
    }

    fun updateProducts(newProducts: List<Product>): List<Product>? {
        var list = ArrayList<Product>()
        products.value?.let { list.addAll(it) }
        list.addAll(newProducts)
        return list
    }

    fun getCategories() {
        viewModelScope.launch {
            categories.value = repository.getAllCategories()
        }
    }

    fun getProductByCategory(category: String, isFirst : Boolean): List<Product>? {
        currentCategory=category
        isFirstCategory =isFirst
      return if(isFirst) products.value else products.value?.let {
            it.filter { product -> product.category.equals(category) }
        }
    }

    fun getProductFiltered(sTitle: String): List<Product>? {
        var lista = products.value?.let {
            it.filter { product -> product.title.contains(sTitle) && (product.category.equals(currentCategory)|| isFirstCategory) }
        }
        return lista
    }

    fun plus(product: Product) {
        if (product.quantity <= 100) {
            products.value= products.value?.map { it ->
                if(it.id == product.id) {
                    it.quantity = product.quantity
                }
                it
            }
            computingOverview()
        }
    }

    fun substrac(product: Product) {
        if (product.quantity >= 0) {
            products.value= products.value?.map { it ->
                if(it.id == product.id) {
                    it.quantity = product.quantity
                }
                it
            }
            computingOverview()
        }
    }

    fun computingOverview(){
        var total = 0.0
        var quantity = 0
        products.value?.forEach { product ->
            if (product.quantity > 0) {
                total += (product.price * product.quantity)
                quantity++
            }
        }
        overview.value = Pair(total, quantity)
    }

    fun clearShop() {
        products.value?.forEach { product ->
            product.quantity = 0
        }
    }

    fun getSelectedproducts() = products.value?.filter { it.quantity > 0 }

    fun delete(id: Int, productsList: List<Product>): List<Product>? {
        var list = ArrayList<Product>()
        productsList.forEach {
            if (id != it.id)
                list.add(it)
        }
        return list
    }

    fun getProducts() = products.value?.filter { it.category.equals(currentCategory) || isFirstCategory}



}