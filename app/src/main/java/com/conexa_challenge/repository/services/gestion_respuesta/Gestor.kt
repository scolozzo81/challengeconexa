


import retrofit2.HttpException

/**
 * Class to handler errors of service with coroutines
 */
object Gestor {

  fun <T : Any> handleSuccess(data: T): Contenedor<T> {
        return Contenedor.success(data)
    }

    fun <T : Any> handleException(e: Exception): Contenedor<T> {
        return when (e) {
            is HttpException -> Contenedor.error(TiposErrorAPI.EXCEPCION_HTTP, null)
            else -> Contenedor.error(TiposErrorAPI.ERROR_GENERAL, null)
        }
    }
}
