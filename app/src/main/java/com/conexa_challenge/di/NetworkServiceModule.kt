package com.conexa_challenge.di

import com.conexa_challenge.repository.services.interfaces.NetworkService
import com.conexa_challenge.repository.services.Service
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkServiceModule {
    @Singleton
    @Binds
    abstract fun getNetworkService(
        implNetworkService: Service
    ): NetworkService
}

