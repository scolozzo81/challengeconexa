package com.conexa_challenge.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.conexa_challenge.db.entities.Product
import com.conexa_challenge.db.converters.RatingConverter
import com.conexa_challenge.db.ProductDAO

@Database(entities = [Product::class],version = 1, exportSchema = false )
@TypeConverters(RatingConverter::class)
abstract class ShopDb  : RoomDatabase(){
   abstract fun productDAO(): ProductDAO
}