package com.conexa_challenge.db.converters

import com.conexa_challenge.repository.services.pojos.principales.Rating
import androidx.room.TypeConverter
import org.json.JSONObject

class RatingConverter {
    @TypeConverter
    fun fromSource(rating: Rating): String{
        return JSONObject().apply {
            put("rate", rating.rate.toString())
            put("count", rating.count.toString())
        }.toString()
    }

    @TypeConverter
    fun toRating(sRating: String): Rating {
        val json = JSONObject(sRating)
        return Rating(json.getString("rate").toDouble(), json.getString("count").toInt())
    }
}