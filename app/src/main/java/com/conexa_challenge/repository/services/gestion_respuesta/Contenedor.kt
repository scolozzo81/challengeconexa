

data class Contenedor<out T>(val status: EstadoRespuesta, val data: T?, val type: TiposErrorAPI?) {
    companion object {
        fun <T> success(data: T?): Contenedor<T> {
            return Contenedor(
                EstadoRespuesta.SUCCESS,
                data,
                null
            )
        }

        fun <T> error(type: TiposErrorAPI, data: T?): Contenedor<T> {
            return Contenedor(
                EstadoRespuesta.ERROR,
                data,
                type
            )
        }

        fun <T> loading(data: T?): Contenedor<T> {
            return Contenedor(
                EstadoRespuesta.LOADING,
                data,
                null
            )
        }
    }
}
