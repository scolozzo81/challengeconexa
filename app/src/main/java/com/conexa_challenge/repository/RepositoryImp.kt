package com.conexa_challenge.repository


import android.text.format.DateUtils
import com.conexa_challenge.db.ShopDb
import com.conexa_challenge.db.entities.Product
import com.conexa_challenge.repository.services.interfaces.NetworkService
import com.conexa_challenge.repository.services.interfaces.Repository
import com.conexa_challenge.utils.ConectionCheck
import com.conexa_challenge.utils.setDateProducts

import kotlinx.coroutines.runBlocking

import javax.inject.Inject


class RepositoryImp @Inject constructor(
    private val db: ShopDb,
    private val service: NetworkService,
    private val comprobarConexion: ConectionCheck
) : Repository {

    override suspend fun getAllProducts(): List<Product> {
        var products: ArrayList<Product>
        var cacheOutDate: Boolean
        var thereAreProducts: Boolean
        var newestDateProducts:Long
        var sizeProductesTable :Long

        sizeProductesTable = db.productDAO().getAmountProducts()

        runBlocking {
            products = ArrayList<Product>()
            newestDateProducts =   if (sizeProductesTable == 0L) 0 else db.productDAO().getLastDateProducts()
            thereAreProducts = sizeProductesTable > 0
            cacheOutDate = newestDateProducts <= 0 || !DateUtils.isToday(newestDateProducts)

            products = when {
                thereAreProducts && !cacheOutDate ->
                    db.productDAO().getAllProducts() as ArrayList<Product>
                else -> getProductsAPI()

            }
        }
        return products
    }

    private fun getProductsAPI(): ArrayList<Product> {
        var products = ArrayList<Product>()
        runBlocking {
            if (comprobarConexion.connectionIsAvailable()) {
                try {
                    var respuesta = Gestor.handleSuccess(service.getAllProducts())
                    if (respuesta.status==EstadoRespuesta.SUCCESS) {
                            respuesta.data?.let { result ->
                                if (!result.isNullOrEmpty()) {
                                    products = result as ArrayList<Product>
                                    db.productDAO().insertProduct(products.setDateProducts())
                                }
                            }
                        }
                } catch (e: Exception) {
                }
            }
        }
        return products
    }

    override suspend fun getAllCategories(): List<String> {
        var categories = ArrayList<String>()
        runBlocking {
            if (comprobarConexion.connectionIsAvailable()) {
                try {
                    var respuesta = Gestor.handleSuccess(service.getAllCategories())
                    if(respuesta.status== EstadoRespuesta.SUCCESS) {
                            respuesta.data?.let { result ->
                                if (!result.isNullOrEmpty()) {
                                    categories = result as ArrayList<String>
                                }
                            }
                        }
                } catch (e: Exception) {  }
            }
        }
        return categories
    }
}





